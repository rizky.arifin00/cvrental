package com.aplication.rizky.cvrental.POJO;

/**
 * Created by rizky on 05/10/17.
 */

public class Transaksi {
    private int id;
    private String idKendaraan;
    private String namaUser;
    private String nohp;
    private String namaKendaraan;
    private int dibayar;
    private int sisa;

    public Transaksi(int id, String idKendaraan, String namaUser, String nohp, String namaKendaraan, int dibayar, int sisa) {
        this.id = id;
        this.idKendaraan = idKendaraan;
        this.namaUser = namaUser;
        this.nohp = nohp;
        this.namaKendaraan = namaKendaraan;
        this.dibayar = dibayar;
        this.sisa = sisa;
    }

    public Transaksi(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdKendaraan() {
        return idKendaraan;
    }

    public void setIdKendaraan(String idKendaraan) {
        this.idKendaraan = idKendaraan;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public String getNohp() {
        return nohp;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getNamaKendaraan() {
        return namaKendaraan;
    }

    public void setNamaKendaraan(String namaKendaraan) {
        this.namaKendaraan = namaKendaraan;
    }

    public int getDibayar() {
        return dibayar;
    }

    public void setDibayar(int dibayar) {
        this.dibayar = dibayar;
    }

    public int getSisa() {
        return sisa;
    }

    public void setSisa(int sisa) {
        this.sisa = sisa;
    }
}
