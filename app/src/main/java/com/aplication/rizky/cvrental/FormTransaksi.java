package com.aplication.rizky.cvrental;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aplication.rizky.cvrental.POJO.Kendaraan;
import com.aplication.rizky.cvrental.POJO.Transaksi;
import com.aplication.rizky.cvrental.databinding.ActivityFormTransaksiBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FormTransaksi extends AppCompatActivity {

    DatabaseReference myRef,myRef2;
    ActivityFormTransaksiBinding binding;
    String id, harga, tgl, namaKend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_form_transaksi);
        binding.setFormtransaksi(this);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            id = b.getString("id");
            harga = b.getString("harga");
            tgl = b.getString("tgl");
        }

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        myRef2 = mRootRef;
        myRef = mRootRef.child("kendaraan");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Kendaraan kendaraan = postSnapshot.getValue(Kendaraan.class);
                    if (kendaraan.getId() == Integer.parseInt(id)) {
                        namaKend = kendaraan.getNama();
                        binding.ketMobil.setText(namaKend);
                        binding.hargaForm.setText(harga);
                        binding.ketDate.setText(tgl);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        binding.submitPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.inputNama.length() != 0 || binding.inputNoHP.length() != 0) {
                    Transaksi transaksi = new Transaksi(1, id, binding.inputNama.getText().toString(),
                            binding.inputNoHP.getText().toString(), namaKend, 100000, Integer.parseInt(harga) - 100000);
                    myRef2.child("transaksi").push().setValue(transaksi);
                    finish();
                    startActivity(new Intent(FormTransaksi.this, HomeActivity.class));
                    Toast.makeText(FormTransaksi.this, "Booking berhasil cek selengkapnya di halaman transaksi", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(FormTransaksi.this, "Isi data terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.batalPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(FormTransaksi.this, HomeActivity.class));
                Toast.makeText(FormTransaksi.this, "Anda batal membooking", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
