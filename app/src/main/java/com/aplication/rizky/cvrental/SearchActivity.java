package com.aplication.rizky.cvrental;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aplication.rizky.cvrental.POJO.Kendaraan;
import com.aplication.rizky.cvrental.databinding.ActivitySearchBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    SearchActivity searchActivity;
    ActivitySearchBinding binding;

    String query;
    List<String> searchArray;
    ArrayAdapter adapter;
    DatabaseReference myRef;
    Intent resultIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        binding.setSearch(searchActivity);

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        myRef = mRootRef.child("search");

        resultIntent = new Intent();
        searchArray = new ArrayList<>();
        adapter = new ArrayAdapter(this, R.layout.list_search, searchArray);
        binding.listSearch.setDivider(null);
        binding.listSearch.setAdapter(adapter);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String search = postSnapshot.getValue(String.class);
                    searchArray.add(search);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        binding.listSearch.setAdapter(adapter);

        binding.listSearch.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                resultIntent.putExtra("query", searchArray.get(position));
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

        binding.clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myRef.removeValue();
                searchArray.clear();
                adapter.notifyDataSetChanged();
            }
        });

        binding.searchQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    query = binding.searchQuery.getText().toString();
                    myRef.push().setValue(query);
                    resultIntent.putExtra("query", query);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
                return false;
            }
        });

        binding.btnclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchQuery.setText("");
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

