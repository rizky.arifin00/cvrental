package com.aplication.rizky.cvrental.Fragment;

import android.app.FragmentTransaction;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aplication.rizky.cvrental.Font.FontManager;
import com.aplication.rizky.cvrental.R;
import com.aplication.rizky.cvrental.databinding.FragmentHomeBinding;
import com.bumptech.glide.Glide;

public class Home extends Fragment {
    TextView tool;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentHomeBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View view = binding.getRoot();

        tool = (TextView) getActivity().findViewById(R.id.toolbar_title);
        tool.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        tool.setText("Home");

        Glide.with(this).load(R.drawable.car2).into(binding.banner);

        Typeface iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Roboto-Regular.ttf");
        Typeface typeBold = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Roboto-Bold.ttf");

        binding.headerPromo.setTypeface(typeBold);
        binding.textPromo.setTypeface(type);
        binding.btnJelajahi.setTypeface(type);
        binding.iRental.setTypeface(iconFont);
        binding.iTransaksi.setTypeface(iconFont);

        binding.btnJelajahi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RentalForm fragment = new RentalForm();
                replaceFragment(fragment);
            }
        });

        binding.iRental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RentalForm fragment = new RentalForm();
                replaceFragment(fragment);
            }
        });

        binding.iTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaksi fragment = new FragmentTransaksi();
                replaceFragment(fragment);
            }
        });

        binding.btnRental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RentalForm fragment = new RentalForm();
                replaceFragment(fragment);
            }
        });

        binding.btnTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaksi fragment = new FragmentTransaksi();
                replaceFragment(fragment);
            }
        });

        return view;
    }

    private void replaceFragment (Fragment fragment){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        android.support.v4.app.FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

}
