package com.aplication.rizky.cvrental.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aplication.rizky.cvrental.DetailKendaraan;
import com.aplication.rizky.cvrental.POJO.Kendaraan;
import com.aplication.rizky.cvrental.R;

import java.util.List;

/**
 * Created by rizky on 16/09/17.
 */

public class KendaraanAdapter extends RecyclerView.Adapter<KendaraanAdapter.viewHolder>{

    Context c;
    int jam;
    String tgl;
    List<Kendaraan> kendaraanlist;

    public KendaraanAdapter(Context c, List<Kendaraan> kendaraanlist, int jam, String tgl) {
        this.c = c;
        this.kendaraanlist = kendaraanlist;
        this.jam = jam;
        this.tgl = tgl;
    }


    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.item_list, parent, false);
        return new viewHolder(v);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {
        final Kendaraan kendaraan = kendaraanlist.get(position);
        holder.tvNamamobil.setText(kendaraan.getNama());
        holder.tvBensin.setText(kendaraan.getBensin());
        holder.tvHarga.setText((kendaraan.getHarga()*jam)+"");
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c.getApplicationContext(), DetailKendaraan.class);
                intent.putExtra("id", kendaraan.getId()+"");
                intent.putExtra("harga", (kendaraan.getHarga()*jam)+"");
                intent.putExtra("tgl", tgl);
                c.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kendaraanlist.size();
    }

    class viewHolder extends RecyclerView.ViewHolder {

        TextView tvNamamobil,tvBensin,tvHarga;
        RelativeLayout item;
        public viewHolder(View itemView) {
            super(itemView);

            item = (RelativeLayout) itemView.findViewById(R.id.itemCard);
            tvNamamobil = (TextView) itemView.findViewById(R.id.namaMobil);
            tvBensin = (TextView) itemView.findViewById(R.id.takaranBensin);
            tvHarga = (TextView) itemView.findViewById(R.id.harga);
        }
    }
}
