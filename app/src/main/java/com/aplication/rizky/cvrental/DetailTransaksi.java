package com.aplication.rizky.cvrental;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aplication.rizky.cvrental.POJO.Kendaraan;
import com.aplication.rizky.cvrental.POJO.Transaksi;
import com.aplication.rizky.cvrental.databinding.ActivityDetailKendaraanBinding;
import com.aplication.rizky.cvrental.databinding.ActivityDetailTransaksiBinding;
import com.aplication.rizky.cvrental.databinding.ActivityFormTransaksiBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailTransaksi extends AppCompatActivity {

    DatabaseReference myRef;
    ActivityDetailTransaksiBinding binding;
    String id, harga, tgl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_detail_transaksi);
        binding.setDetailTran(this);
        Bundle b = getIntent().getExtras();
        if (b != null){
            id = b.getString("id");
        }

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        myRef = mRootRef.child("transaksi");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Transaksi transaksi = postSnapshot.getValue(Transaksi.class);
                    if (transaksi.getId() == Integer.parseInt(id)){
                        binding.dibayar.setText(transaksi.getDibayar()+"");
                        binding.namaKend.setText(transaksi.getNamaKendaraan());
                        binding.sisa.setText(transaksi.getSisa()+"");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
