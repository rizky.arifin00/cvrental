package com.aplication.rizky.cvrental.RecylerList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aplication.rizky.cvrental.Adapter.TransaksiAdapter;
import com.aplication.rizky.cvrental.POJO.Transaksi;
import com.aplication.rizky.cvrental.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ListProsesTransaksi extends Fragment {
    TransaksiAdapter adapter;
    RecyclerView recyclerView;
    List<Transaksi> kendaraan;
    ProgressDialog progressDialog;
    TextView tool;

    public ListProsesTransaksi() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_rental_kendaraan, container, false);

        tool = (TextView) getActivity().findViewById(R.id.toolbar_title);
        tool.setText("Transaksi");

        progressDialog = new ProgressDialog(getActivity());

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("transaksi");

        recyclerView = (RecyclerView) view.findViewById(R.id.recylerKendaraan1);
        kendaraan = new ArrayList<Transaksi>();

        adapter = new TransaksiAdapter(getActivity(),kendaraan, 0, null);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        progressDialog.setMessage("Loading ..");
        progressDialog.show();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Transaksi kend = postSnapshot.getValue(Transaksi.class);
                    kendaraan.add(kend);
                    adapter.notifyDataSetChanged();
                };
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.out.println(error.toException());
            }
        });

        return view;
    }

}
