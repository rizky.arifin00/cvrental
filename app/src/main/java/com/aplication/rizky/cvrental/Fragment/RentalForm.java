package com.aplication.rizky.cvrental.Fragment;

import android.app.FragmentTransaction;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.aplication.rizky.cvrental.R;
import com.aplication.rizky.cvrental.databinding.FragmentHomeBinding;
import com.aplication.rizky.cvrental.databinding.FragmentRentalFormBinding;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RentalForm extends Fragment implements View.OnClickListener {

    TextView tool;
    String tgl,bulan;
    Date currentTime, dateBegin, dateEnd;
    SimpleDateFormat fmtDay, fmtDate, fmtTime, fmtTgl;
    FragmentRentalFormBinding binding;
    int jam;

    public RentalForm() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rental_form, container, false);
        View view = binding.getRoot();

        tool = (TextView) getActivity().findViewById(R.id.toolbar_title);
        tool.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        tool.setText("Rental Mobil");
//        now = Calendar.getInstance();
        currentTime = Calendar.getInstance().getTime();
        dateBegin = currentTime;
        dateEnd = currentTime;
        fmtDay = new SimpleDateFormat("EEEE");
        fmtTgl = new SimpleDateFormat("dd");
        fmtDate = new SimpleDateFormat("dd MMM yyyy");
        fmtTime = new SimpleDateFormat("HH:mm");

        binding.btnDateKiri.setOnClickListener(this);
        binding.btnDate.setOnClickListener(this);
        binding.btnTime.setOnClickListener(this);
        binding.btnTimeKiri.setOnClickListener(this);
        binding.btnCari.setOnClickListener(this);

        setDateTime(currentTime);
        difDate(dateBegin,dateEnd);
        return view;
    }

    public void setDateTime(Date dateTime) {
        binding.hariAwal.setText(fmtDay.format(dateTime).toString());
        binding.hariAkhir.setText(fmtDay.format(dateTime).toString());
        binding.tglAwal.setText(fmtDate.format(dateTime).toString());
        binding.tglAkhir.setText(fmtDate.format(dateTime).toString());
        binding.textJam.setText(fmtTime.format(dateTime).toString());
        binding.textJamKiri.setText(fmtTime.format(dateTime).toString());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnDateKiri) {
            final Calendar now = Calendar.getInstance();
            final DatePickerDialog dpd = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                            String date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            now.set(year, monthOfYear, dayOfMonth);
                            dateBegin = now.getTime();
                            binding.hariAwal.setText(fmtDay.format(dateBegin).toString());
                            binding.tglAwal.setText(fmtDate.format(dateBegin).toString());

                            tgl = fmtTgl.format(dateBegin);
                            difDate(dateBegin,dateEnd);
                        }
                    },
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        } else if (v.getId() == R.id.btnDate) {
            final Calendar now = Calendar.getInstance();
            final DatePickerDialog dpd = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                            String date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            now.set(year, monthOfYear, dayOfMonth);
                            dateEnd = now.getTime();

                            binding.hariAkhir.setText(fmtDay.format(dateEnd).toString());
                            binding.tglAkhir.setText(fmtDate.format(dateEnd).toString());

                            difDate(dateBegin,dateEnd);
                        }
                    },
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        } else if (v.getId() == R.id.btnTimeKiri) {
            final Calendar now = Calendar.getInstance();
            TimePickerDialog dpd = TimePickerDialog.newInstance(
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                            String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
                            String minuteString = minute < 10 ? "0" + minute : "" + minute;
                            String secondString = second < 10 ? "0" + second : "" + second;
                            String time = hourString + ":" + minuteString;

                            dateBegin.setHours(hourOfDay);
                            dateBegin.setMinutes(minute);

                            binding.textJamKiri.setText(time);

                            difDate(dateBegin,dateEnd);
                        }
                    },
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    true
            );
            dpd.show(getActivity().getFragmentManager(), "TimePickerDialog");
        } else if (v.getId() == R.id.btnTime) {
            final Calendar now = Calendar.getInstance();
            TimePickerDialog dpd = TimePickerDialog.newInstance(
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                            String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
                            String minuteString = minute < 10 ? "0" + minute : "" + minute;
                            String secondString = second < 10 ? "0" + second : "" + second;
                            String time = hourString + ":" + minuteString;

                            dateEnd.setHours(hourOfDay);
                            dateEnd.setMinutes(minute);

                            binding.textJam.setText(time);

                            difDate(dateBegin,dateEnd);
                        }
                    },
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    true
            );
            dpd.show(getActivity().getFragmentManager(), "TimePickerDialog");
        } else if (v.getId() == R.id.btnCari){
            Rental rental = new Rental();
            Bundle bundle = new Bundle();
            bundle.putInt("jumjam", jam);
            bundle.putString("fulldate", binding.tglAwal.getText().toString());
            bundle.putString("date", tgl);
            rental.setArguments(bundle);
            replaceFragment(rental);
        }
    }

    public void difDate(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        binding.difDate.setText((elapsedDays*24)+elapsedHours+" Jam");
        jam = (int) ((elapsedDays*24)+elapsedHours);
//        System.out.printf(
//                "%d days, %d hours, %d minutes, %d seconds%n",
//                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
    }

    private void replaceFragment (Fragment fragment){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        android.support.v4.app.FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null || backStateName.equals(Rental.class.getName())){ //fragment not in back stack, create it.
            android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
