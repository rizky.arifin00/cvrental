package com.aplication.rizky.cvrental.Font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by rizky on 13/09/17.
 */

public class FontManager {
    public static final String ROOT = "fonts/",
            FONTAWESOME = ROOT + "fontawesome-webfont.ttf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }
}
