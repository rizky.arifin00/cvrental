package com.aplication.rizky.cvrental.POJO;

/**
 * Created by rizky on 13/09/17.
 */

public class Kendaraan {
    private int id;
    private String nama;
    private String jenis;
    private String no_plat;
    private String foto;
    private String status;
    private String cv;
    private int harga;
    private String bensin;

    public Kendaraan(){

    }

    public Kendaraan(int id, String nama, String jenis, String no_plat, String foto, int harga, String status, String cv, String bensin) {
        this.id = id;
        this.nama = nama;
        this.jenis = jenis;
        this.no_plat = no_plat;
        this.foto = foto;
        this.harga = harga;
        this.status = status;
        this.cv = cv;
        this.bensin = bensin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getNo_plat() {
        return no_plat;
    }

    public void setNo_plat(String no_plat) {
        this.no_plat = no_plat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getBensin() {
        return bensin;
    }

    public void setBensin(String bensin) {
        this.bensin = bensin;
    }
}
