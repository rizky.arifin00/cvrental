package com.aplication.rizky.cvrental.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aplication.rizky.cvrental.DetailTransaksi;
import com.aplication.rizky.cvrental.POJO.Transaksi;
import com.aplication.rizky.cvrental.R;

import java.util.List;

/**
 * Created by rizky on 07/10/17.
 */

public class TransaksiAdapter extends RecyclerView.Adapter<TransaksiAdapter.viewHolder>{

    Context c;
    int jam;
    String tgl;
    List<Transaksi> kendaraanlist;

    public TransaksiAdapter(Context c, List<Transaksi> kendaraanlist, int jam, String tgl) {
        this.c = c;
        this.kendaraanlist = kendaraanlist;
        this.jam = jam;
        this.tgl = tgl;
    }

    @Override
    public TransaksiAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.recycle_list, parent, false);
        return new TransaksiAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(TransaksiAdapter.viewHolder holder, int position) {
        final Transaksi transaksi = kendaraanlist.get(position);
        holder.tvMobil.setText(transaksi.getNamaKendaraan());
        holder.tvHarga.setText("Rp."+transaksi.getDibayar()+"");
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c.getApplicationContext(), DetailTransaksi.class);
                intent.putExtra("id", transaksi.getId()+"");
                c.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kendaraanlist.size();
    }

    class viewHolder extends RecyclerView.ViewHolder {

        TextView tvMobil,tvStatus,tvHarga;
        LinearLayout item;
        public viewHolder(View itemView) {
            super(itemView);

            item = (LinearLayout) itemView.findViewById(R.id.itemTransaksi);
            tvMobil = (TextView) itemView.findViewById(R.id.tvMobil);
            tvStatus = (TextView) itemView.findViewById(R.id.lblstatus);
            tvHarga = (TextView) itemView.findViewById(R.id.lblHarga);
        }
    }
}
