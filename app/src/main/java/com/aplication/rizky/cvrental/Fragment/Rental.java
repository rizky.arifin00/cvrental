package com.aplication.rizky.cvrental.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aplication.rizky.cvrental.Adapter.KendaraanAdapter;
import com.aplication.rizky.cvrental.Font.FontManager;
import com.aplication.rizky.cvrental.HomeActivity;
import com.aplication.rizky.cvrental.POJO.Kendaraan;
import com.aplication.rizky.cvrental.R;
import com.aplication.rizky.cvrental.SearchActivity;
import com.aplication.rizky.cvrental.databinding.FragmentHomeBinding;
import com.aplication.rizky.cvrental.databinding.FragmentRentalBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;


public class Rental extends Fragment {

    KendaraanAdapter adapter;
    List<Kendaraan> kendaraan;
    Kendaraan kend;
    ProgressDialog progressDialog;
    TextView tool;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    DatabaseReference myRef;
    FragmentRentalBinding binding;
    Drawable img;
    String fulldate;
    int date;
    Date datefull;
    SimpleDateFormat fmtFullDate,fmtDate;

    public Rental() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rental, container, false);
        View view = binding.getRoot();

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        myRef = mRootRef.child("kendaraan");

        progressDialog = new ProgressDialog(getActivity());
        kendaraan = new ArrayList<Kendaraan>();

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_main);
        tool = (TextView) getActivity().findViewById(R.id.toolbar_title);
        drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        if (bundle != null) {
            int jam = bundle.getInt("jumjam", 0);
            fulldate = bundle.getString("fulldate", null);
            date = Integer.parseInt(bundle.getString("date", null));
            adapter = new KendaraanAdapter(getActivity(), kendaraan, jam, fulldate);
        } else {
            adapter = new KendaraanAdapter(getActivity(), kendaraan, 0, null);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        img = getContext().getResources().getDrawable(R.drawable.ic_search_black_24dp);
        img.setBounds(0, 0, 36, 36);

        tool.setText("Rental");
        binding.emptySearch.setVisibility(View.GONE);
        binding.recylerKendaraan1.setLayoutManager(linearLayoutManager);
        binding.recylerKendaraan1.setAdapter(adapter);
        progressDialog.setMessage("Loading ..");
        progressDialog.show();


        datefull = new Date();
        fmtFullDate = new SimpleDateFormat("dd MMM yyyy");
        fmtDate = new SimpleDateFormat("dd");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    kend = postSnapshot.getValue(Kendaraan.class);
                    if (!kend.getStatus().contains("Available")) {
                        try {
                            datefull = fmtFullDate.parse(kend.getStatus());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (date > Integer.parseInt(fmtDate.format(datefull))) {
                            kendaraan.add(kend);
                        }
                    } else {
                        kendaraan.add(kend);
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.out.println(error.toException());
            }
        });

        binding.etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SearchActivity.class);
                startActivityForResult(i, 1);
            }
        });

        binding.clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.clear.setVisibility(View.GONE);
                binding.emptySearch.setVisibility(View.GONE);
                binding.etSearch.setHint("Search by name");
                binding.etSearch.setCompoundDrawables(img, null, null, null);
                kendaraan.clear();
                progressDialog.show();
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            kend = postSnapshot.getValue(Kendaraan.class);
                            if (!kend.getStatus().contains("Available")) {
                                try {
                                    datefull = fmtFullDate.parse(kend.getStatus());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if (date > Integer.parseInt(fmtDate.format(datefull))) {
                                    kendaraan.add(kend);
                                }
                            } else {
                                kendaraan.add(kend);
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_menu: {
                showDialog();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            final String query = extras.getString("query");
            kendaraan.clear();
            binding.clear.setVisibility(View.VISIBLE);
            binding.etSearch.setPadding(8, 16, 8, 16);
            binding.etSearch.setCompoundDrawables(null, null, null, null);
            progressDialog.show();
            myRef.orderByChild("nama").startAt(query).endAt(query + "\uf8ff").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    progressDialog.dismiss();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        kend = postSnapshot.getValue(Kendaraan.class);
                        if (!kend.getStatus().contains("Available")) {
                            try {
                                datefull = fmtFullDate.parse(kend.getStatus());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (date > Integer.parseInt(fmtDate.format(datefull))) {
                                kendaraan.add(kend);
                            }
                        } else {
                            kendaraan.add(kend);
                        }
                        adapter.notifyDataSetChanged();
                    }
                    binding.etSearch.setHint(query);
                    if (kendaraan.size() == 0) {
                        binding.emptySearch.setVisibility(View.VISIBLE);
                        binding.emptySearch.setText("Tidak ditemukan item yang memiliki nama " + query);

                    } else {
                        binding.emptySearch.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println(databaseError);
                }
            });
        }
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.filter_dialog);
        dialog.setTitle("Filter Data");

        final Button btnFilter = (Button) dialog.findViewById(R.id.btnFilter);
        final TextView txtRating = (TextView) dialog.findViewById(R.id.textRating);
        final TextView txtHarga = (TextView) dialog.findViewById(R.id.textHarga);

        final LinearLayout rating = (LinearLayout) dialog.findViewById(R.id.Rating);
        final LinearLayout harga = (LinearLayout) dialog.findViewById(R.id.Harga);

        final RadioButton tertinggi = (RadioButton) dialog.findViewById(R.id.tertinggi);
        final RadioButton terendah = (RadioButton) dialog.findViewById(R.id.rendah);

        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rating.setBackgroundColor(Color.parseColor("#00a5f3"));
                txtRating.setTextColor(Color.parseColor("#ffffff"));
                tertinggi.setText("Rating tertinggi");
                terendah.setText("Rating terendah");

                harga.setBackgroundColor(Color.parseColor("#ffffff"));
                txtHarga.setTextColor(Color.parseColor("#000000"));
            }
        });

        harga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                harga.setBackgroundColor(Color.parseColor("#00a5f3"));
                txtHarga.setTextColor(Color.parseColor("#ffffff"));
                tertinggi.setText("Harga tertinggi");
                terendah.setText("Harga terendah");

                rating.setBackgroundColor(Color.parseColor("#ffffff"));
                txtRating.setTextColor(Color.parseColor("#000000"));
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                kendaraan.clear();
                if (terendah.isChecked()) {
                    if (terendah.getText().toString().equals("Harga terendah")) {
                        myRef.orderByChild("harga").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    kend = postSnapshot.getValue(Kendaraan.class);
                                    kendaraan.add(kend);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                } else {
                    myRef.orderByChild("harga").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                kend = postSnapshot.getValue(Kendaraan.class);
                                kendaraan.add(kend);
                            }
                            Collections.reverse(kendaraan);
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        dialog.show();
    }

}
