package com.aplication.rizky.cvrental;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aplication.rizky.cvrental.POJO.Kendaraan;
import com.aplication.rizky.cvrental.databinding.ActivityDetailKendaraanBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailKendaraan extends AppCompatActivity {

    DatabaseReference myRef;
    ActivityDetailKendaraanBinding binding;
    String id, harga, tgl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_detail_kendaraan);
        binding.setDetailkend(this);
        Bundle b = getIntent().getExtras();
        if (b != null){
            id = b.getString("id");
            harga = b.getString("harga");
            tgl = b.getString("tgl");
        }

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        myRef = mRootRef.child("kendaraan");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Kendaraan kendaraan = postSnapshot.getValue(Kendaraan.class);
                    if (kendaraan.getId() == Integer.parseInt(id)){
                        binding.textNamaMobil.setText(kendaraan.getNama());
                        binding.textRupiah.setText(harga);
                        binding.textCv.setText(kendaraan.getCv());
                        binding.textBensin.setText(kendaraan.getBensin());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        binding.buttonBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailKendaraan.this, FormTransaksi.class);
                intent.putExtra("id", id);
                intent.putExtra("harga", harga);
                intent.putExtra("tgl", tgl);
                finish();
                startActivity(intent);
            }
        });
    }
}
